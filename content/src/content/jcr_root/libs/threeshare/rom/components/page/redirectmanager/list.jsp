<%--
Copyright 2012 - 3|SHARE Corporation
All Rights Reserved.

This software is the confidential and proprietary information of
3|SHARE Corporation, ("Confidential Information").

==============================================================================
--%><%@include file="/libs/foundation/global.jsp"%><%@page import="com.day.cq.search.*,
  com.day.cq.search.result.SearchResult,
  com.day.cq.search.result.Hit,
  org.apache.sling.api.resource.ResourceUtil,
  org.apache.sling.api.resource.ValueMap,
  java.util.HashMap,
  org.json.JSONObject,
  java.util.List,
  java.util.ArrayList,
  java.util.Arrays,
  java.util.Map,
  com.threeshare.rom.util.ROMConstants,
  com.threeshare.rom.search.SearchUtil,
  org.apache.jackrabbit.JcrConstants,
  com.threeshare.rom.vanity.*,
  com.threeshare.rom.util.RequestUtils"%><%


  /*************************************************************************
   * Initializations
   *************************************************************************/

  String headerText = currentPage.getTitle() == null ? xssAPI.encodeForHTML(currentPage.getName()) : xssAPI.encodeForHTML(currentPage.getTitle());
  String customVanityProperty = ROMConstants.VANITY_TYPE_CUSTOM_JCR_PROPERTY;
  String vanityProperty = ROMConstants.VANITY_TYPE_STANDARD_JCR_PROPERTY;
  String defaultRedirectType = ROMConstants.DEFAULT_REDIRECT_TYPE;
  String vanityURLKey = ROMConstants.VANITY_URI_PAGE_PROPERTY_KEY;
  String redirectTypeKey = ROMConstants.REDIRECT_TYPE_PAGE_PROPERTY_KEY;


  /*************************************************************************
   * Read Query Parameters
   *************************************************************************/

  //Set Default Content Path
  String content = "/content";
  String contentParam = RequestUtils.getParameter(request,"content");
  content = contentParam == null ? content : contentParam;

  // Set Rewrite Parameters
  String rewriteParam = "";

  //Set Remove Path, used for removing content path to match Resource Resolver rules
  String removePath = "";

  //Set Exclusion Paths from comma separated exclude parameter
  String excludePathsParam = "";

  //Set Append Path, used for append content path to match Resource Resolver rules
  String defaultAppendPath = "";

  //Set Vanity type
  String vanityType = "standard";

  String configPath = currentNode.getPath() + "/redirects";

  /*************************************************************************
   * Find and Output Vanities
   *************************************************************************/
%>
<style>
  tr:nth-child(even) {background-color: #f2f2f2}
  table { width: 100%; border-collapse: collapse; }
  th { height: 50px; }
  table, th, td { border: 1px solid black; }
  th { height: 30px; }
</style>
<%=headerText%><br>
<table>
<tr>
  <th>Vanity</th>
  <th>Target</th>
<th>Redirect Type</th>
</tr>



<%


  VanityConfigNode configNode = VanityFactory.getConfigNode(resourceResolver, configPath);
List<VanityNode> vanitiesList = configNode.getVanities();

  for (VanityNode vanity : vanitiesList)
  {
      String vanityURI = vanity.getVanityURI();
      String vanityTarget = vanity.getVanityTarget();
      String redirectType = vanity.getRedirectType();

      if (vanityURI.length() > 0 && vanityURI != null)
      {%>
            <tr>
              <td><%=vanityURI%></td>
              <td><%=vanityTarget%></td>
              <td><%=redirectType%></td>
            </tr>
  <%

      }
  }

%>
</table>

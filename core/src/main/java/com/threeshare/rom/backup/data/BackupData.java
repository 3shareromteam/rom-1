package com.threeshare.rom.backup.data;

public class BackupData {
	private Boolean backupWasSuccessful;
	private Boolean backupInProgress;
    private Integer backupProgress;
    private String backupResult;
    private String backupTarget;

    public Boolean getBackupInProgress() {
        return backupInProgress;
    }

    public void setBackupInProgress(Boolean backupInProgress) {
        this.backupInProgress = backupInProgress;
    }

    public Integer getBackupProgress() {
        return backupProgress;
    }

    public void setBackupProgress(Integer backupProgress) {
        this.backupProgress = backupProgress;
    }

    public String getBackupResult() {
        return backupResult;
    }

    public void setBackupResult(String backupResult) {
        this.backupResult = backupResult;
    }

    public Boolean getBackupWasSuccessful() {
        return backupWasSuccessful;
    }

    public void setBackupWasSuccessful(Boolean backupWasSuccessful) {
        this.backupWasSuccessful = backupWasSuccessful;
    }

	public void setBackupTarget(String backupTarget) {
        this.backupTarget = backupTarget;
	}
	
	public String getBackupTarget() {
        return backupTarget;
	}
	
	public String toXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<prtg>");
		
		sb.append("<result>");
		sb.append("<channel>Backup In Progress</channel>");
		sb.append("<Unit>Custom</Unit>");
		sb.append("<CustomUnit>inProgress</CustomUnit>");
        sb.append("<value>" + (this.backupInProgress? 1 : 0) + "</value>");
		sb.append("</result>");
		
		sb.append("<result>");
		sb.append("<channel>Last Backup Successful</channel>");
		sb.append("<unit>Custom</unit>");
		sb.append("<CustomUnit>wasSuccessful</CustomUnit>");
		sb.append("<value>" + (this.backupWasSuccessful? 1 : 0) + "</value>");
		sb.append("</result>");
		
		sb.append("<result>");
		sb.append("<channel>Backup Progress</channel>");
		sb.append("<unit>Percent</unit>");
		sb.append("<value>" + this.backupProgress/10 + "</value>");
		sb.append("</result>");
		
		sb.append("<text>" + this.backupResult + "</text>");
		sb.append("</prtg>");
		return sb.toString();
	}
	
}

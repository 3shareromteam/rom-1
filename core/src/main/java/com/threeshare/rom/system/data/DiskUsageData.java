package com.threeshare.rom.system.data;

public class DiskUsageData {
	private long capacity;
	private long usage;
	
	public void setCapacity(long capacity) {
		this.capacity = capacity;
	}
	
	public long getCapacity() {
		return this.capacity;
	}
	
	public void setUsage(long usage) {
		this.usage = usage;
	}
	
	public long getUsage() {
		return this.usage;
	}
	
	public long getFreeDisk() {
		return this.capacity - this.usage;
	}
	
	public String toXML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<prtg>");
		sb.append("<result>");
		sb.append("<channel>Disk Capacity</channel>");
		sb.append("<unit>BytesDisk</unit>");
		sb.append("<volumeSize>Byte</volumeSize>");
		sb.append("<value>" + this.capacity + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Disk Usage</channel>");
		sb.append("<unit>BytesDisk</unit>");
		sb.append("<volumeSize>Byte</volumeSize>");
		sb.append("<value>" + this.usage + "</value>");
		sb.append("</result>");
		sb.append("<result>");
		sb.append("<channel>Free Disk</channel>");
		sb.append("<unit>BytesDisk</unit>");
		sb.append("<volumeSize>Byte</volumeSize>");
		sb.append("<value>" + this.getFreeDisk() + "</value>");
		sb.append("</result>");
		sb.append("</prtg>");
		return sb.toString();
	}
}

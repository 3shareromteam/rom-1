package com.threeshare.rom.workflow.impl;

import com.threeshare.rom.workflow.ROMWorkflow;
import com.threeshare.rom.workflow.data.WorkflowData;
import com.threeshare.rom.workflow.data.WorkflowDataList;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
@Service
@Properties({
    @Property(name = "service.description", value = "ROM Workflow Data Collector"),
    @Property(name = "service.vendor", value = "3|SHARE")
})
/***
 * ROM Workflow Implementation.
 */
public class ROMWorkflowImpl implements ROMWorkflow {

    private final Logger log = LoggerFactory.getLogger(getClass());
    @Reference
    private ResourceResolverFactory resolverFactory;

    @Override
    public String getWorkflowsInError() throws Exception {
        //-------------------------------------------------------------------
        // Find workflows that appear to be in an error state.  Those
        // usually have an empty history node.  Get a list of those under
        // /etc/workflow/instances that are running and have an empty
        // history node.
        //-------------------------------------------------------------------

        //-------------------------------------------------------------------
        // Search for workflows in a running status that have no values
        // under the history node.
        //-------------------------------------------------------------------
        ResourceResolver resolver = null;
       /* try {
            resolver = resolverFactory.getAdministrativeResourceResolver(null);
            QueryBuilder qb = resolver.adaptTo(QueryBuilder.class);
            log.info("qm = " + qm);
            WorkflowDataList list = new WorkflowDataList();
            String select = "SELECT * FROM [cq:Workflow] AS s WHERE ISDESCENDANTNODE([/etc/workflow/instances]) AND status='RUNNING'";
            Query query = qb.createQuery(select, Query.JCR_SQL2);
            QueryResult result = query.execute();
            NodeIterator nodeIter = result.getNodes();
            while (nodeIter.hasNext()) {
                Node node = nodeIter.nextNode();
                if (node.hasNode("history")) {
                    Node historyNode = node.getNode("history");
                    if (!historyNode.hasNodes()) {
                        //-------------------------------------------------------
                        // This is usually the error condition.
                        //-------------------------------------------------------
                        WorkflowData data = new WorkflowData();
                        data.setNodeName(node.getIdentifier());
                        data.setInitiator(node.getProperty("initiator").getString());
                        data.setStartTime(node.getProperty("startTime").getDate());
                        if (node.hasProperty("data/metaData/userid")) {
                            data.setUserId(node.getProperty("data/metaData/userid").getString());
                        }
                        if (node.hasProperty("data/payload/path")) {
                            data.setResourcePath(node.getProperty("data/payload/path").getString());
                        }
                        list.addWorkflowData(data);
                    }
                }

            }

            return list.toXML();
        } finally {
            if (resolver != null) {
                resolver.close();
            }
        }*/
         throw new UnsupportedOperationException("Not supported yet.");
   }

    /**
     * *
     *
     * @param model - Optional. If defined, only instances using this model will
     * be evaluated.
     * @param timeout - Required. Time in minutes that defines how long a
     * workflow is running before it is considered an error
     * @return Returns XML for instances above the timeout threshold.
     * @throws Exception
     */
    public String getLongStandingWorkflows(String model, int timeout) throws Exception {
        //-------------------------------------------------------------------
        // Find workflows that have been running for too long.  
        //-------------------------------------------------------------------

        //-------------------------------------------------------------------
        // Get nodes under /etc/workflow/instances that have a status=RUNNING.
        //-------------------------------------------------------------------

        //-------------------------------------------------------------------
        // Compare the start time to the timeout.  If the timeout has passed,
        // add this to the list of returned workflows.  Before adding the item,
        // look up the model from the modelId.  Use the jcr:title of the model.
        //-------------------------------------------------------------------

        //-------------------------------------------------------------------
        // Return the values as a prtg xml.
        //-------------------------------------------------------------------

        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getRunningWorkflowCount() throws Exception {
        //-------------------------------------------------------------------
        // Search for a list of workflows that are in a running status.
        // Simply return the count.
        //-------------------------------------------------------------------

        throw new UnsupportedOperationException("Not supported yet.");
    }
}

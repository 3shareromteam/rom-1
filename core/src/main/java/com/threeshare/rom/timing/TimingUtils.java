package com.threeshare.rom.timing;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestProgressTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.threeshare.rom.timing.ChartBar;
import com.threeshare.rom.util.ROMConstants;

public class TimingUtils
{
	private static final Logger log = LoggerFactory.getLogger(TimingUtils.class);
	private static final String TIMER_END = "TIMER_END";
	public static String test1 = "1";
	
	public static String getComponentTiming (HttpServletRequest request, String requestType)
	{
		String timingData = "";
		if (request instanceof SlingHttpServletRequest)
		{
			SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
			timingData = getComponentTiming(slingRequest, requestType);
		}
		else
		{
			log.info("Not a valid request type for timing.");
		}
		
		return timingData;
		
	}
	
	public static String getComponentTiming (ServletRequest request, String requestType)
	{
		String timingData = "";
		if (request instanceof SlingHttpServletRequest)
		{
			SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
			timingData = getComponentTiming(slingRequest, requestType);
		}
		else
		{
			log.info("Not a valid request type for timing.");
		}
		
		return timingData;
		
	}
	
	public static String getComponentTiming (SlingHttpServletRequest slingRequest, String requestType)
	{
		String componentData = "No Data";

		RequestProgressTracker tracker = slingRequest.getRequestProgressTracker();
		ArrayList<ChartBar> chartData = new ArrayList<ChartBar>();
		
		int maxTime = 0;
		for(Iterator<String> it = tracker.getMessages() ; it.hasNext(); ) 
		{
			String line = it.next();
			log.debug(line);
			
			if(accept(line)) 
			{
			    ChartBar b = new ChartBar(line);
			    chartData.add(b);
			    maxTime = b.end;
			}
		}
		
		// Sort data according to numeric start time
		Collections.sort(chartData, ChartBar.sortChartBar);
				
		if (requestType != null && requestType.equals(ROMConstants.JSON_REQUEST_TYPE))
		{
			componentData = getJsonComponentData(chartData);
		}
		else
		{
			// Default to String, which defaults to google chart api
			componentData = getGoogleComponentData(chartData, maxTime);
		}
			
		return componentData;
	}
	
	@SuppressWarnings("deprecation")
	private static String getGoogleComponentData(ArrayList<ChartBar> chartData, int maxTime) {
		
		String componentData = null;
		
		// Chart API limitations - max 30k pixels!
		int chartWidth = 600;
		int maxPixels = 300000;
		int chartHeight = maxPixels / chartWidth;

		// See http://code.google.com/apis/chart/types.html#bar_charts for docs
		StringBuilder b = new StringBuilder();
		b.append("http://chart.apis.google.com/chart");
		b.append("?chtt=" + URLEncoder.encode("3|SHARE ROM Data"));
		b.append("&cht=bhs");
		b.append("&chxt=x");
		b.append("&chco=c6d9fd,4d89f9");
		b.append("&chbh=a");
		b.append("&chds=0," + maxTime + ",0," + maxTime);
		b.append("&chxr=0,0," + maxTime);
		b.append("&chd=t:");
		b.append(stringList(chartData, ",","start"));
		b.append("|");
		b.append(stringList(chartData, ",","end"));
		b.append("&chly=");
		b.append(stringList(chartData, "|","name"));
		b.append("&chs=" + chartWidth + "x" + chartHeight);
		
		componentData = b.toString();
		
		return componentData;
		
	}
	
	@SuppressWarnings("unused")
	private static String getJsonComponentData(ArrayList<ChartBar> chartData) {
		
		// Using stringbuilder instead of JSONObject due custom data format
		StringBuilder b = new StringBuilder();
		b.append("[");
		
		
		// Add column headers
		String column1Title = "\"Genre\"";
		String column2Title = "\"Waiting (ms)\"";
		String column3Title = "\"Loading (ms)\"";

		b.append("[");
		b.append(column1Title);
		b.append(",");
		b.append(column2Title);
		b.append(",");
		b.append(column3Title);
		b.append("]");
		
		for (ChartBar bar : chartData)
		{
			b.append(",");
			b.append("[");
			b.append("\"" + bar.get("name") + "\"");
			b.append(",");
			b.append(bar.get("start"));
			b.append(",");
			b.append(bar.get("end"));
			b.append("]");
		}
		
		b.append("]");
		String componentData = b.toString();
		
		return componentData;
	}

	private static boolean accept(String line) 
	{
	    boolean result = line.contains(TIMER_END);
	    result &= !line.contains(",resolveServlet(");
	    result &= !line.contains("ResourceResolution");
	    result &= !line.contains("ServletResolution");
	    return result;
	}

	@SuppressWarnings("deprecation")
	public static String stringList(List<ChartBar> data, String separator, String key)
	{
		StringBuilder result = new StringBuilder();
		
		for(ChartBar t : data) {
			if(result.length() > 0) 
			{
				result.append(separator);
			}
			result.append(URLEncoder.encode(t.get(key)));
		}
		return result.toString();
	}
}
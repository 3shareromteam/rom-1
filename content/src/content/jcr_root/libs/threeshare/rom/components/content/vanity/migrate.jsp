<%--
  Copyright 2012 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

  ==============================================================================
--%><%@include file="/libs/foundation/global.jsp"%><%@page import="com.day.cq.search.*,
    com.day.cq.search.result.SearchResult, 
    com.day.cq.search.result.Hit,
    com.day.cq.commons.jcr.JcrConstants,
    org.apache.sling.api.resource.ResourceUtil, 
    org.apache.sling.api.resource.ValueMap,
    java.util.List,
    java.util.ArrayList,
    java.util.HashMap, 
    java.util.Map"%><%

// This is used to migrate from the traditional vanity URL configuration
// to the custom vanity format.
    
String customVanityProperty = "sling:vanityPathCustom";
String vanityProperty = "sling:vanityPath";
String defaultRedirectType = "302";
String vanityURLKey = "vanity";
String redirectTypeKey = "redirectType";
boolean save = "true".equals(request.getParameter("save"));
    
// Set Content Path
String defaultContentPath = "/content";
String content = request.getParameter("content");
if (content == null || content.equals(""))
{
    content = defaultContentPath;
}

// Set Rewrite Parameters
String defaultRewriteParam = "R";
String rewriteParam = request.getParameter("param");
if (rewriteParam == null || rewriteParam.equals(""))
{
    rewriteParam = defaultRewriteParam;
}

//Set Remove Path, used for removing content path to match Resource Resolver rules
String defaultRemovePath = "";
String removePath = request.getParameter("removePath");
if (removePath == null || removePath.equals(""))
{
    removePath = defaultRemovePath;
}

//adapt to the QueryBuilder and Session
QueryBuilder queryBuilder = resourceResolver.adaptTo(QueryBuilder.class);
Session session = resourceResolver.adaptTo(Session.class);

Map map = new HashMap();
map.put("path", content);
map.put("p.limit", "100000");
map.put("type", "cq:Page");
map.put("property", "@jcr:content/sling:vanityPath");

Query query = queryBuilder.createQuery(PredicateGroup.create(map), session);
String xPath = PredicateGroup.create(map).toString();
SearchResult result = query.getResult();

long totalMatches = result.getTotalMatches();

if (totalMatches > 0)
{
   for (Hit hit : result.getHits())
    {
        Resource hResource = hit.getResource();
        if (hResource != null)
        {
            ValueMap props = ResourceUtil.getValueMap(hResource);
            String[] vanityUrls = props.get("jcr:content/" + vanityProperty ,String[].class);
            Page hPage = hResource.adaptTo(Page.class);
            if (hPage != null && vanityUrls != null)
            {
                String path = hPage.getPath();
                List<String> vanityList = new ArrayList<String>();
                
                for(String vanityUrl : vanityUrls)
                {
                    if (vanityUrl.length() > 0 && vanityUrl != null)
                    {
                    	String jsonString = "{\"" + vanityURLKey + "\":\"" + vanityUrl + "\",\"" + redirectTypeKey + "\":\"" + defaultRedirectType + "\"}";
                    	vanityList.add(jsonString);
                    }
                }
                
                if (vanityList.size() > 0)
                {
                	for (String vanity : vanityList)
            		{
            			%>Updating <%=path %>/jcr:content/<%=customVanityProperty %> to <%=vanity %><br><%
            		}
                	
                	if (save)
                	{
                		String[] vanityArray = vanityList.toArray(new String[vanityList.size()]);
	                	Node pageNode = hResource.adaptTo(Node.class);
	                	Node contentNode = pageNode.getNode(JcrConstants.JCR_CONTENT);
	                	contentNode.setProperty(customVanityProperty, vanityArray);
	                	contentNode.getSession().save();
	                	%>Property Saved<br><%
                	}
                	else
                	{
                		%>Property Save Disabled<br><%
                	}
                }
                
                
            }
        }
    }
}

%>

package com.threeshare.rom.monitor.revisioncleanup.impl;

import com.threeshare.rom.monitor.revisioncleanup.RevisionCleanupMonitor;
import com.threeshare.rom.monitor.revisioncleanup.data.RevisionCleanupMonitorData;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import org.apache.felix.scr.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
@Service
@Properties({
    @Property(name = "service.description", value = "3|SHARE ROM Revision Cleanup Monitor Data Collector"),
    @Property(name = "service.vendor", value = "3|SHARE")
})
public class RevisionCleanupMonitorImpl implements RevisionCleanupMonitor {

    @Reference
    private MBeanServer mbeanServer;
    private final Logger log = LoggerFactory.getLogger(getClass());

    public String getRevisionMonitorData() throws Exception {
        try {
            String getRevisionMonitorDataResult = (String) mbeanServer.getAttribute(new ObjectName("org.apache.sling.healthcheck:type=HealthCheck,name=MaintenanceTaskRevisionCleanupTask"), "status");
            RevisionCleanupMonitorData revisionCleanupMonitorData = new RevisionCleanupMonitorData();
            revisionCleanupMonitorData.setBackupTarget(getRevisionMonitorDataResult);
            return revisionCleanupMonitorData.toXML();
        } catch (Exception e) {
            log.error("", e);
            throw e;
        }
    }

}

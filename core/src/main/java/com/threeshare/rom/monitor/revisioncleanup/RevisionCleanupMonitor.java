package com.threeshare.rom.monitor.revisioncleanup;

public interface RevisionCleanupMonitor {
    public String getRevisionMonitorData() throws Exception;
}

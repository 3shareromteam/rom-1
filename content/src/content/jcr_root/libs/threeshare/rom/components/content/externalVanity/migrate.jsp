<%--
  Copyright 2012 - 3|SHARE Corporation
  All Rights Reserved.

  This software is the confidential and proprietary information of
  3|SHARE Corporation, ("Confidential Information").

  ==============================================================================
--%>
<%@ include file="/libs/foundation/global.jsp" %>
<%@ page import="com.threeshare.rom.config.ROMConfigManager" %>
<%@ page import="com.threeshare.rom.util.ROMConstants" %>
<%@ page import="org.apache.sling.api.request.RequestPathInfo" %>
<%@ page import="com.threeshare.rom.vanity.VanityFactory"%>
<%@ page import="com.threeshare.rom.vanity.ConfigNode"%>
<%@ page import="com.threeshare.rom.vanity.VanityNode"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>

<%
// This script is used to move from the legacy single page vanity to the modular
// version.

String configNode = "/set/path/here";

// Get Vanity configuration node
ConfigNode configNode = VanityFactory.getConfigNode(resourceResolver, configNode);
boolean invalidConfig = configNode == null;

ROMConfigManager configManager = new ROMConfigManager(resourceResolver);
String[] vanityURLs = configManager.getMultiConfig(ROMConfigManager.EXTERNAL_VANITY);
String vanityDelimiter = ROMConstants.VANITY_DELIMITER;
String redirectDelimiter = ROMConstants.REDIRECT_DELIMITER;

if (invalidConfig)
{
    %>Invalid Config Node<%
}
else if (vanityURLs.length <= 0)
{ 
    %>No legacy vanities found<%	
}
else
{
	for(String vanityURL : vanityURLs)
	{
	    if (vanityURL.length() > 0 && vanityURL.contains(vanityDelimiter) && vanityURL.contains(redirectDelimiter))
	    {
	        String vanity = vanityURL.substring(0, vanityURL.indexOf(vanityDelimiter)).trim();
	        String targetAndRedirect = vanityURL.substring(vanityURL.indexOf(vanityDelimiter) + vanityDelimiter.length()).trim();
	        String target = targetAndRedirect.substring(0, targetAndRedirect.indexOf(redirectDelimiter)).trim();
	        String redirectType = targetAndRedirect.substring(targetAndRedirect.indexOf(redirectDelimiter) + redirectDelimiter.length()).trim();
	        
	        VanityNode newVanity = configNode.createVanity(vanity, target, redirectType);
	        if (newVanity != null){
	        	%>Vanity created for <%=vanity%> at <%=newVanity.getPath()%><br><%
	        }
	               
	    }    
	}
}
%>
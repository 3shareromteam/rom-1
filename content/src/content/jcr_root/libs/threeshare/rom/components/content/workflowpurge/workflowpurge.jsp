<%@include file="/libs/foundation/global.jsp" %><%
%><%@page import="java.util.List" %><%
%><%@page import="java.util.ArrayList" %><%
%><%@page import="org.apache.sling.api.resource.ResourceUtil" %><%
%><%@page import="javax.jcr.Node" %><%
%><%@page import="javax.jcr.NodeIterator" %><%
%><%@page import="com.threeshare.rom.util.NodeUtils" %><%    
%><%@page import="com.threeshare.rom.util.RequestUtils" %><%


%><%



// Save Parameter    
boolean save = "true".equals(request.getParameter("delete"));    

// maxDelete Parameter
String maxPurgeParameter = "maxPurgeCount";    
String maxWorkFlowToPurgeStr = RequestUtils.getParameter(request, maxPurgeParameter);
int maxWorkFlowToPurge = 10;
if (maxWorkFlowToPurgeStr != null && maxWorkFlowToPurgeStr.matches("^\\d+$") && Integer.valueOf(maxWorkFlowToPurgeStr) != null)
{
	maxWorkFlowToPurge = Integer.valueOf(maxWorkFlowToPurgeStr);
}
%><%=maxWorkFlowToPurge%><%

// maxDepth Parameter
String maxDepthParameter = "maxDepth";    
String maxDepthStr = RequestUtils.getParameter(request, maxDepthParameter);
int maxDepth = 2;
if (maxDepthStr != null && maxDepthStr.matches("^\\d+$") && Integer.valueOf(maxDepthStr) != null)
{
	maxDepth = Integer.valueOf(maxDepthStr);
}

// searchPath Parameter
String searchPathParameter = "searchPath";    
String searchPathQuery = RequestUtils.getParameter(request, searchPathParameter);
String workflowInstancesNodePath = "/etc/workflow/instances";
if (searchPathQuery != null && searchPathQuery.length() > 0)
{
	workflowInstancesNodePath = searchPathQuery;
}

int maxLevelIn = 1;
int count = 0;

List<String> deletePaths = NodeUtils.crawlNodes(resourceResolver,workflowInstancesNodePath,maxWorkFlowToPurge,maxDepth);

for (String deletePath : deletePaths)
{
    %>Looking for <%=deletePath %>...<br><%
    Resource delResource = resourceResolver.getResource(deletePath);
    if (delResource != null)
    {    
        Node delNode = delResource.adaptTo(Node.class);
        %>Found Node <%=deletePath %>.<br><%
		%>Node Name <%=delNode.getName() %>.<br><%

        if (save && delNode != null)
        {
            delNode.remove();
            delNode.getSession().save();
            %>Deleted Node at <%=deletePath %>.<br><br><%   
        }
        else
        {
            %>Delete not enabled.<br><br><%
        }
    }
    else
    {
        %>Node <%=deletePath%> Not Found.<br><br><%
    }
}
%>

Node Removal Complete.

